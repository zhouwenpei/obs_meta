Name:           openEuler-latest-release
Summary:        System information like kernelversion, openeulerversion, gccversion, openjdkversion and compile time
License:        GPL
Group:          System/Config
Version:        1.0
%ifarch aarch64
Release:        %(date +%s -d "$(grep "compiletime" %{_sourcedir}/isopackage_arm64.sdf | cut -d '=' -f2 | sed 's/-/ /3' | sed 's/-/:/3' | sed 's/-/:/3')")%(grep "openeulerversion" %{_sourcedir}/isopackage_arm64.sdf | cut -d '-' -f2)
%else
Release:        %(date +%s -d "$(grep "compiletime" %{_sourcedir}/isopackage.sdf | cut -d '=' -f2 | sed 's/-/ /3' | sed 's/-/:/3' | sed 's/-/:/3')")%(grep "openeulerversion" %{_sourcedir}/isopackage.sdf | cut -d '-' -f2)
%endif
BuildRoot:      %{_tmppath}/%{name}-%{version}

%description
The rpm contains system information, like kernelversion, eulerversion and compile time so on.

%setup -q

%install
mkdir -p %{buildroot}/etc
%ifarch aarch64
install %{_sourcedir}/isopackage_arm64.sdf %{buildroot}/etc/openEuler-latest
%else
install %{_sourcedir}/isopackage.sdf %{buildroot}/etc/openEuler-latest
%endif

%pre

%post
if [[ `grep "openeulerversion" /etc/openEuler-latest | cut -d '_' -f2` =~ 2\.2\.RC.* ]]; then
    if [ $1 = 1 ]; then
	if [ -e /etc/openEulerLinux.conf ];then
	    mv /etc/openEulerLinux.conf /etc/.openEulerLinux.conf
	fi
    else
	rm -f /etc/.openEulerLinux.conf
    fi
fi

%preun

%postun
if [ -e /etc/.openEulerLinux.conf ]; then
mv /etc/.openEulerLinux.conf /etc/openEulerLinux.conf
fi

%files
%config /etc/openEuler-latest
%attr(0444, root, root) /etc/openEuler-latest

%clean
rm -rf $RPM_BUILD_ROOT/*
rm -rf %{_tmppath}/%{name}-%{version}
rm -rf $RPM_BUILD_DIR/%{name}-%{version}

%changelog
